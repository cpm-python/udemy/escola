import requests
from datetime import datetime

now = datetime.now()
dt_string = now.strftime("%d-%m-%Y_%H-%M-%S")

header = {'Authorization': 'Token 7ac4a33f6a689fff953cb9f112ec9af281f922e4'}
header_autenticado = {'Authorization': 'Token f3d338c432cc61b7a51cc57507e443eedbff618f'}
urn = 'http://localhost:8000/api/v2/'
uri_cursos = f'{urn}cursos/'
uri_avaliacoes = f'{urn}avaliacoes/'

atual_curso = {
    "titulo": "Atual titulo " + dt_string,
    "url": uri_cursos + dt_string
}

novo_curso = {
    "titulo": "Novo titulo " + dt_string,
    "url": uri_cursos + dt_string
}
resultado_curso = requests.post(url=uri_cursos, headers=header, data=novo_curso)

# testando status
assert resultado_curso.status_code == 201

id_inserido = resultado_curso.json()['id']

uri_curso = f'{uri_cursos}{id_inserido}/'

# Get Cursos
curso = requests.get(url=uri_curso, headers=header)


# Validando sucesso
assert curso.status_code == 200

# Valida se o valor é diferente do novo
assert curso.json()['titulo'] != atual_curso['titulo']

cursos = requests.put(url=uri_curso, headers=header, data=atual_curso)

# Validando forbiden
assert cursos.status_code == 403

cursos = requests.put(url=uri_curso, headers=header_autenticado, data=atual_curso)

# Validando sucesso
assert cursos.status_code == 200

# Valida se foi atualizado
assert cursos.json()['titulo'] == atual_curso['titulo']
cursos = requests.delete(url=f'{uri_curso}', headers=header_autenticado)

# Validando sucesso
assert cursos.status_code == 204

assert len(cursos.text) == 0
