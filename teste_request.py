import requests


#Get Avaliações
avaliacoes = requests.get('http://localhost:8000/api/v2/avaliacoes/')

# Status http
print(avaliacoes.status_code) # 405

# Dados resposta
print(avaliacoes.json())
print(type(avaliacoes.json())) # Dicionario Python

# Detalhes
print(avaliacoes.json()['detail'])


# Get Avaliação
avaliacao = requests.get('http://localhost:8000/api/v2/avaliacoes/5/')

# Status
print(avaliacao.status_code) # 200

# Dados resposta
print(avaliacao.json())
print(type(avaliacao.json())) # Dicionario Python

header = {'Authorization': 'Token 7ac4a33f6a689fff953cb9f112ec9af281f922e4'}

# Get Cursos
cursos = requests.get(url='http://localhost:8000/api/v2/cursos/', headers=header)

# Status http
print(cursos.status_code) # 200

# Dados resposta
print(cursos.json())
print(type(cursos.json())) # Dicionario Python

# Quantidade de registro
print(cursos.json()['count'])

# Pegando a proxima pagina
print(cursos.json()['next'])

# Pegando os resultados da pagina
print(cursos.json()['results'])
print(type(print(cursos.json()['results'])))

# Pegando o primeiro elemento
print(cursos.json()['results'][0])

# Pegando o ultimo elemento
print(cursos.json()['results'][-1])

# Pegando o titulo do ultimo elemento
print(cursos.json()['results'][-1]['titulo'])

