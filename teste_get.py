import requests

header = {'Authorization': 'Token 7ac4a33f6a689fff953cb9f112ec9af281f922e4'}
urn = 'http://localhost:8000/api/v2/'
uri_cursos = urn + 'cursos/'
uri_avaliacoes = urn + 'avaliacoes/'


# Get Cursos
cursos = requests.get(url=uri_cursos, headers=header)
print(cursos.json())

# Validando sucesso
assert cursos.status_code == 200

# Validando quantidade de registros
assert cursos.json()['count'] == 8

# Testando o titulo
assert cursos.json()['results'][0]['titulo'] == 'Curso 11'

# Get Avaliações
avaliacoes = requests.get(url=uri_avaliacoes, headers=header)
print(avaliacoes.json())

# Validando 405
assert avaliacoes.status_code == 405

# Validando a detail
assert avaliacoes.json()['detail'] == 'Método "GET" não é permitido.'

