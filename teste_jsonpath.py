import requests
import jsonpath


header = {'Authorization': 'Token 7ac4a33f6a689fff953cb9f112ec9af281f922e4'}

# Get Cursos
cursos = requests.get(url='http://localhost:8000/api/v2/cursos/', headers=header)

resultados = jsonpath.jsonpath(cursos.json(), 'results')
print(resultados)

primeiro = jsonpath.jsonpath(cursos.json(), 'results[0]')
print(primeiro)

titulo = jsonpath.jsonpath(cursos.json(), 'results[0].titulo')
print(titulo)

media_avaliacoes = jsonpath.jsonpath(cursos.json(), 'results[0].media_avaliacoes')
print(media_avaliacoes)

# Todos os titulos
titulos = jsonpath.jsonpath(cursos.json(), 'results[*].titulo')
print(titulos)
