import requests
from datetime import datetime

now = datetime.now()
dt_string = now.strftime("%d-%m-%Y_%H-%M-%S")


header = {'Authorization': 'Token 7ac4a33f6a689fff953cb9f112ec9af281f922e4'}
header_autenticado = {'Authorization': 'Token f3d338c432cc61b7a51cc57507e443eedbff618f'}
urn = 'http://localhost:8000/api/v2/'
uri_cursos = f'{urn}cursos/'
uri_avaliacoes = f'{urn}avaliacoes/'
uri_curso = f'{uri_cursos}21/'

atual_curso = {
    "titulo": "Novo titulo " + dt_string,
    "url": uri_cursos + dt_string
}

# Get Cursos
curso = requests.get(url=uri_curso, headers=header)


# Validando sucesso
assert curso.status_code == 200

# Valida se o valor é diferente do novo
assert curso.json()['titulo'] != atual_curso['titulo']

cursos = requests.put(url=uri_curso, headers=header, data=atual_curso)


# Validando forbiden
assert cursos.status_code == 403

cursos = requests.put(url=uri_curso, headers=header_autenticado, data=atual_curso)

# Validando sucesso
assert cursos.status_code == 200

# Valida se foi atualizado
assert cursos.json()['titulo'] == atual_curso['titulo']