import requests
from datetime import datetime

now = datetime.now()
dt_string = now.strftime("%d-%m-%Y_%H-%M-%S")

urn = 'http://localhost:8000/api/v2/'


class TestCurso:
    header_autenticado = {'Authorization': 'Token f3d338c432cc61b7a51cc57507e443eedbff618f'}
    uri_cursos = f'{urn}cursos/'
    id_para_mudar = f'{uri_cursos}42/'

    def test_get_cursos(self):
        # Get Cursos
        cursos = requests.get(url=self.uri_cursos, headers=self.header_autenticado)
        print(cursos.json())

        # Validando sucesso
        assert cursos.status_code == 200

    def test_get_curso(self):
        print(f'{self.uri_cursos}{self.id_para_mudar}/')
        # Get Curso
        curso = requests.get(url=self.id_para_mudar, headers=self.header_autenticado)
        print(curso.json())

        # Validando sucesso
        assert curso.status_code == 200

    def test_post_cursos(self):
        novo_curso = {
            "titulo": "Novo titulo " + dt_string,
            "url": self.uri_cursos + dt_string
        }

        resultado_curso = requests.post(url=self.uri_cursos, headers=self.header_autenticado, data=novo_curso)
        print(resultado_curso.json())

        # testando status
        assert resultado_curso.status_code == 201
        print(resultado_curso.json()['id'])

    def test_put_cursos(self):
        atual_curso = {
            "titulo": "Atual titulo " + dt_string,
            "url": self.id_para_mudar
        }

        resultado_put = requests.put(url=self.id_para_mudar, headers=self.header_autenticado, data=atual_curso)

        # Validando sucesso
        assert resultado_put.status_code == 200

        # Valida se foi atualizado
        assert resultado_put.json()['titulo'] == atual_curso['titulo']

    def test_delete_cursos(self):
        resultado_delete = requests.delete(url=self.id_para_mudar, headers=self.header_autenticado)

        # Validando sucesso
        assert resultado_delete.status_code == 204 and len(resultado_delete.text) == 0
