import requests
from datetime import datetime

now = datetime.now()
dt_string = now.strftime("%d-%m-%Y_%H-%M-%S")


header = {'Authorization': 'Token 7ac4a33f6a689fff953cb9f112ec9af281f922e4'}
urn = 'http://localhost:8000/api/v2/'
uri_cursos = urn + 'cursos/'
uri_avaliacoes = urn + 'avaliacoes/'


novo_curso = {
    "titulo": "Novo titulo " + dt_string,
    "url": uri_cursos + dt_string
}

print(novo_curso)

resultado_curso = requests.post(url=uri_cursos, headers=header, data=novo_curso)

# testando status
assert resultado_curso.status_code == 201

# testando titulo
assert resultado_curso.json()['titulo'] == novo_curso['titulo']

# testando 2 urls iguais
resultado_curso2 = requests.post(url=uri_cursos, headers=header, data=novo_curso)

# testando status
assert resultado_curso2.status_code == 400
